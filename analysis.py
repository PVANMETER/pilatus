#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This module exists to facilitate real-time and post-processing of ME-SXR data.
"""

from __future__ import print_function

import os
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import tifffile as tif
import calibration as calib

def load_data(shot, base_path='/home/patrick/Mount/data/meSXR/images/MST_data', remove_edges=True, device='MST'):
    """
    Indexed by data[x_index, y_index, time]
    """
    file_list = get_file_list(shot, base_path)
    data = np.ma.masked_array(np.zeros([calib.M_SIZE_X, calib.M_SIZE_Y, len(file_list)]))

    # Mask bad pixels
    if remove_edges:
        masked_pixels = get_edge_mask()
    else:
        included_pixels = calib.get_included_pixels(key=device)
        masked_pixels = np.logical_not(included_pixels)

    for index, fname in enumerate(file_list):
        data[:, :, index] = np.ma.masked_array(tif.imread(fname).T, mask=masked_pixels)
    
    return data

def slideshow(shot, vmin=0, vmax=80, xmax=15000., ymax = 20000.,
              base_path='/home/patrick/Mount/data/meSXR/images/MST_data', remove_edges=False):
    """
    Credit to matplotlib.org for the basis of this function.
    """
    data = load_data(shot, base_path=base_path, remove_edges=remove_edges)
    fig = plt.figure(1, figsize=(8,6), dpi=110)
    
    tracker = IndexTracker(fig, data, vmin=vmin, vmax=vmax, xmax=xmax, ymax=ymax, shot=shot)
    fig.canvas.mpl_connect('scroll_event', tracker.onscroll)
    plt.show()

def get_file_list(shot, base_path, skip_index=[]):
    """
    Utility function to load in generate the list of files corresponding to a given
    ME-SXR acquisition.
    """
    data_path = os.path.join(base_path, str(shot/1000), str(shot))

    # Load files in directory to determine how many frames were taken
    files_in_dir = os.listdir(data_path)
    file_list = []

    index = 0
    while index != -1:
        fname = 's{0:10d}_{1:05d}.tif'.format(shot, index)
        if fname in files_in_dir:
            file_list.append(os.path.join(data_path, fname))
            index += 1
        elif index in skip_index:
            index += 1
        else:
            index = -1

    return file_list

class IndexTracker(object):
    """
    Needed for the slideshow function - generates and updates the plots for fast
    analysis.
    """
    def __init__(self, fig, X, vmin=0, vmax=80, xmax=5000., ymax = 10000., shot=0):
        self.shot = shot
        self.ax = plt.subplot2grid((2,2), (0,0), rowspan=2)
        self.ax.set_title('MST Shot #{0:010d}'.format(shot))

        self.X = X
        rows, cols, self.slices = X.shape
        self.ind = 0

        # Plot 1 - Full image
        self.im = self.ax.imshow(self.X[:, :, self.ind], vmin=vmin, vmax=vmax)
        self.cbar = plt.colorbar(self.im, label='Counts')
        self.ax.set_xlabel('Y pixel')
        self.ax.set_ylabel('X pixel')

        # Plot 2 - X-axis projection
        self.ax2 = plt.subplot2grid((2,2), (0,1))
        image_x_proj = np.ma.sum(self.X[:, :, self.ind], axis=1)
        self.im2, = self.ax2.plot(image_x_proj)
        self.ax2.set_ylim([0, xmax])
        self.ax2.yaxis.tick_right()
        self.ax2.yaxis.set_label_position('right')
        self.ax2.xaxis.tick_top()
        self.ax2.xaxis.set_label_position('top')
        self.ax2.set_xlabel('X pixel')
        self.ax2.set_ylabel(r'Counts')

        # Plot 3 - Y-axis projection
        self.ax3 = plt.subplot2grid((2,2), (1,1))
        image_y_proj = np.ma.sum(self.X[:, :, self.ind], axis=0)
        self.im3, = self.ax3.plot(image_y_proj)
        self.ax3.set_ylim([0, ymax])
        self.ax3.yaxis.tick_right()
        self.ax3.yaxis.set_label_position('right')
        self.ax3.set_xlabel('Y pixel')
        self.ax3.set_ylabel(r'Counts')

        self.update()

    def onscroll(self, event):
        #print("%s %s" % (event.button, event.step))
        if event.button == 'up':
            self.ind = (self.ind + 1) % self.slices
        else:
            self.ind = (self.ind - 1) % self.slices
        self.update()

    def update(self):
        self.im.set_data(self.X[:, :, self.ind])
        self.im.axes.figure.canvas.draw()

        self.im2.set_ydata(np.ma.sum(self.X[:, :, self.ind], axis=1))
        self.im2.axes.figure.canvas.draw()

        self.im3.set_ydata(np.ma.sum(self.X[:, :, self.ind], axis=0))
        self.im3.axes.figure.canvas.draw()

        self.ax.set_ylabel('Image %s' % self.ind)

def get_edge_mask():
    """
    Get a mask that hides all pixels on the edge of chips in addition to the gap region
    """
    # Make the edge mask - for now this is hard-coded
    edge_mask = np.logical_not(calib.get_included_pixels())

    internal_cols = [n*61 - 1 for n in range(1,8)]
    for col in internal_cols:
        edge_mask[col-1, :] = True
        edge_mask[col, :] = True
        edge_mask[col+1, :] = True

    # Now get the far left and right sides
    edge_mask[0, :] = True
    edge_mask[486, :] = True

    # Now the rows
    edge_mask[:, 0] = True
    edge_mask[:, 96] = True
    edge_mask[:, 97] = True
    edge_mask[:, 98] = True
    edge_mask[:, 194] = True
    
    return edge_mask