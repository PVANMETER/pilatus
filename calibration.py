#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Module: calibration_MST
Package: pilatus
Version: 12.17

Summary
================
   Some scripts for dealing with advance calibration schemes for the
   Dectris Pilatus 2 detectors.
Description
================
   To save the trimbit configuration simply run the script:
        ipython -i calibration_MST.py

   To change which configuration is saved, go to the end of this script and
   and change the call to main()

   The folowing functions are contained within this module:

   To update to the most recent version:
   N/A Update soon

   TODO Move geometric constants and detector configurations to a dictionary
Installation
================
Python external dependencies (can install with pip):
    numpy, scipy, matploblib
    PIL (python Imaging Library)
    tifffile
Module Contents
================
    1. 
Authors
================
    Novimir Antoniuk Pablant (Initial version)
        - PPPL
    Jacob Maddox (Version 2)
        - PPPL
    Patrick VanMeter (Current Version)
        - UW-Madison
        - pvanmeter@wisc.edu
Version History
================
12.17 (Dec 2017):
    - Initial version configured for MST calibration data.
    - Initial modular redesign underway.
"""
import logging
import re
import os
import pickle
import numpy as np
import trimfit as trim
import energyfit as enrgy
import configuration as pix

#Configure the logging system
logging.basicConfig(level=logging.INFO)

# Geometric constants
M_NUM_CHIPS_Y = 2
M_NUM_CHIPS_X = 8
M_NUM_CHIPS = M_NUM_CHIPS_X * M_NUM_CHIPS_Y
M_CHIP_SIZE_Y = 97
M_CHIP_SIZE_X = 60
M_SIZE_Y = 195
M_SIZE_X = 487
M_NUM_TRIM = 64

# Camera constants
#bad_pixels = [(333, 2), (280, 55), (12, 71), (376, 94), (198, 117)]
bad_pixels = {'MST':[(333, 2), (280, 55), (12, 71), (376, 94), (198, 117)],
              'DIIID':[(124, 188), (281, 112), (356, 16)]}

#---------------------------------------------------------------------------
def calibrate_detector(settings):
    """
    Description
    ===============
        The main routine of this script which performs the energy fits and generates trimbit
        configuration files.

        This script primarily performs two distinct tasks: 1. Fit X-ray calibration
        count data to S-curves vs. the trimbit setting (the trimbit fit); and 2. Fit the trimbit vs
        energy to a quadratic fit (the energy fit). The function configure_detector from the
        configuration module can only be run once these fits have been performed.

        If the trimbit fit or energy fit has already been performed for the specified data set
        then that step can be skipped.
    Parameters:
    ===============
        - calibration_root_paths = (list of strings) Absolute paths to the root directory of
            each trimscan calibration data set.
        - Output_root_path = (string) Path to the root directory where all output data
            is to be stored.
        - calibration_name = (string) Name for the energy calibration to be performed. This
            determines the directory that the output is to be stored in.
        - excluded = (dictionary) Keys correspond to elements to exclude from the fitting
            procedure. Entries are arries which designate the specific chip numbers the exclusion
            applies to. For instance excluded={'Zr':[2]} excludes Zr from chip 2 only, while 
            excluded={'Zr':[2], 'Mo':[2,3]} also excludes Mo from 2 and 3. By default all "bad"
            pixels and fake pixels are excluded. Also note that if you want to exclude the entire
            detector for an element you may leave the range blank but still include the element.
            So, {'Zr':[]} is equivaluent to {'Zr': range(16)}.
        - start_bit = (dictionary of dictionaries) Trimbit corresponding to the first bit to include in
            the fitting routine. This is a dictionary indexed by element string and theb by chip.
            By default start_bit[elem][2] = 4 and for all other values start_bit[elem][i] = 0. Any
            elements not supplied will revert to these defaults. Here elem is a string like 'Zr',
            'Fe', etc. If the entry for a given element is an integer instead of a dictionary that
            value will be used as the starting bit for all chips, i.e start_bit = {'Fe':4, ...} is
            equivalent to start_bit = {'Fe':{0:4, 1:4, 2:4, ...}, ...}.
        - refit_scurves = (Boolean) Set to False to load existing S-curve (trimbit) fits. This
            allows the user to only perform the energy fit.
        - refit_energy = (Boolean) Set to False to load existing energy fits. This
            allows the user to only perform the trimbit fit.
    Returns:
    ===============
        - calibration = (list of dictionaries)
        - energy_calibration = (dictionary)
    """
    calibrations = []

    # Load in the calibration dictionaries for the desired trimscans
    for trimscan_dir in settings['trimscans'].keys():
        calib_path = os.path.join(settings['calibration_path'], trimscan_dir)
        calibration_directories = get_calibration_files(calib_path,
                                        include_elem=settings['trimscans'][trimscan_dir].keys())

        for elem in calibration_directories.keys():
            calib = get_calibration(calibration_directories[elem])

            # Add calibration settings to the calibration dictionary
            calib['settings'] = settings['trimscans'][trimscan_dir][elem]
            calibrations.append(calib)

    # 1. Fit the trimbit scan data to S-curves, or load in existing data
    for index, calib in enumerate(calibrations):
        # Path to prior fits, if they exist
        scan_name = os.path.split(os.path.split(calib['path'])[0])[1]
        elem_name = os.path.split(calib['path'])[1]
        output_path = make_output_directory(os.path.join('trimfits', scan_name), settings['output_path'])
        fits_filename = os.path.join(output_path, 'MST_{0}.pickle'.format(elem_name))

        # Set up some default settings values if not specified
        if 'exclude_chips' not in calib['settings'].keys():
            calib['settings']['exclude_chips'] = []

        if 'refit_scurve' not in calib['settings'].keys():
            calib['settings']['refit_scurve'] = True

        if 'start_bit' not in calib['settings'].keys():
            calib['settings']['start_bit'] = {}
        
        if 'max_start_bit' not in calib['settings'].keys():
            calib['settings']['max_start_bit'] = 4

        # Refit the S-curves only if desired, or if a previous fit cannot be found
        if not os.path.isfile(fits_filename) or calib['settings']['refit_scurve']:
            # Read in the calibration data images
            calib['trimscan'] = trim.load_calibration_data(calib['path'])

            # Fit all the trimbit S-Curves from data
            logging.info('Fitting trimbit S-Curves for: {}'.format(calib['element']))
            trimfits, trimcov, trimfails, trimstarts = trim.fit_trimbit_scan(calib['trimscan'],
                                                       start_bit=calib['settings']['start_bit'],
                                                       max_start_bit=calib['settings']['max_start_bit'],
                                                       chi_limit=calib['settings']['chi_limit'],
                                                       key=settings['device'])

            calib['trimfits'] = trimfits
            calib['trimcovs'] = trimcov
            calib['trimfails'] = trimfails
            calib['start bits'] = trimstarts

            # Save the fits and covariance matrix as a pickle file for future use
            logging.info('Saving the Trimbit S-Curve fits to: {}'.format(fits_filename))
            pickle.dump(calib, open(fits_filename, 'wb'), -1)

        else:
            # Use previously performed fits and save time
            logging.info('Loading the Trimbit S-Curve fits from: {}'.format(fits_filename))
            calib_pkl = pickle.load(open(fits_filename, 'rb'))
            calib['trimfits'] = calib_pkl['trimfits']
            calib['trimcovs'] = calib_pkl['trimcovs']
            calib['trimfails'] = calib_pkl['trimfails']
            calib['start bits'] = calib_pkl['start bits']
            calib['trimscan'] = calib_pkl['trimscan']

        # Explicitly mark excluded pixels as "trimfails"
        print('For element ' + calib['element'] + ' excluding chips: ' +
              str(calib['settings']['exclude_chips']) + '.')
        included_pixels = get_included_pixels(excluded_chips=calib['settings']['exclude_chips'], key=settings['device'])

        for x_index in range(M_SIZE_X):
            for y_index in range(M_SIZE_Y):
                if not included_pixels[x_index, y_index]:
                    calib['trimfails'][x_index, y_index] = 1

    # 2. Fit the trimbit-energy dependence, or load existing data
    output_path = make_output_directory(os.path.join('enfits', settings['name']), settings['output_path'])
    enfits_filename = os.path.join(output_path, 'MST_energy_fits.pickle')

    if (not os.path.isfile(enfits_filename)) or settings['refit_energy']:
        logging.info('Fitting trimbit energy curves.')
        energyfits, energycov, energyfails = enrgy.fit_energy_dependence(calibrations, key=settings['device'])

        energy_calibration = {'energyfits': energyfits,
                              'energycovs': energycov,
                              'energyfails': energyfails}

        # Save the output file.
        logging.info('Saving the Trimbit-Energy fit to: {}'.format(enfits_filename))
        pickle.dump(energy_calibration, open(enfits_filename, 'wb'), -1)

    else:
        # Use previously performed fits and save time
        logging.info('Loading the Trimbit-Energy fit from: {}'.format(enfits_filename))
        energy_calibration = pickle.load(open(enfits_filename, 'rb'))

    return calibrations, energy_calibration

def make_output_directory(base_name, output_root_path):
    """
    Description
    ===============
        Check if the directory to store output files exists. If not, create it. This creates a
        directory in the output directory with the same name as the folder containing the trimscan
        data.
    Parameters:
    ===============
        - base_name = (string) The name of the folder which calibration data will be stored in. If a
                      full path is passed as this argument only the basename will be used. This allows
                      the user to pass the full path to the calibration data in order to duplicate that
                      top-level directory name for the results.
        - output_root_path = (string) Path pointing to the directory where all calibration data is stored.
        - fits_type = (string) Additional directory name indicating the type of fit, i.e 'trimfits' or
                      'enfits'.
    Returns
    ===============
        - output_files_path = (string) Full path to the output directory.
    """
    output_files_path = os.path.join(output_root_path, base_name)

    if not os.path.exists(output_files_path):
        os.makedirs(output_files_path)

    return output_files_path

def get_calibration_files(calib_path, include_elem):
    """
    Description
    ===============
        Given a path to the calibration directory, return a list of absolute paths to the
        directories contained within it.
    Parameters:
    ===============
        - calibration_path = (string) Path to the top-level directory containing the directories
                corresponding to trimscans for a given line. This directory should contain folders
                with names like calib_line=Mo_vrfp=0.050_vadj=1.160_vtrm=1.250_vcmp=0.800_trim=scan
        - exclude_elem = (list of strings) List of element identifiers to exclude from the calibration.
                         For instance to exclude Zr and Mo, exclude_elem = ['Zr', 'Mo']. Must be a
                         list, even if only one element is being excluded.
    Returns:
    ===============
        - (string[# files]) A list of absolute paths to the calibration trimbit scan data for each
                line.
    """
    cal_files = os.listdir(calib_path)

    # Remove the excluded files - sorry this is not an elegant solution (for now)
    cal_files_included = {}
    for fname in cal_files:
        elem = fname.split('=')[1].split('_')[0]
        if elem in include_elem:
            cal_files_included[elem] = os.path.join(calib_path, fname)

    return cal_files_included

def get_calibration(trimscan_path, autotrim_path=None):
    """
    Description
    ===============
        Given a path to the calibration directory, extract information on the
        calibration settings.
    Parameters:
    ===============
        - trimscan_path = (string) Path to the directory containing the trimscan .tiff files for a
                specific calibration element.
        - autotrim_path = (string) Absolute path to the autotrim files produced by the calibration
                procedure. These are usually stored in the appropriate output path. Loading these
                files is not essential if the trimfit is being performed for the first time, or if
                it is being refit.
    Returns:
    ===============
        - calibration = (dict) Contains information relating to a given trimbit scan, including the
                        element line energy, path to .tif files, and specific detector settings.
    """
    # Dictionary of energies corresponding to emission lines, in keV
    energies = {'Zr': 2.04, 'Mo': 2.29, 'Ag': 2.98, 'In': 3.29, 'Ti': 4.51, 'V': 4.95,
                'Cr': 5.41, 'Fe': 6.40, 'Cu': 8.05, 'Ge': 9.89, 'Br': 11.92, 'Y': 14.95,
                'MoK': 17.48, 'AgK': 22.16, 'Sn': 25.27}

    # Read in the elements string from the path and extract the appropriate energy in keV
    basename = os.path.basename(trimscan_path.rstrip(os.sep))
    elem_string = re.findall('(?<=calib_line=)[^_]+', basename)[0]
    try:
        energy = energies[elem_string]
    except:
        raise Exception('Unknown energy')

    # Read in the calibration settings file from the same directory
    settings_filename = 'setdacs_b01_m01.dat'
    settings = read_settings_file(os.path.join(trimscan_path, settings_filename))

    # Create a dictionary to store information relevent to the calibration
    calibration = {'path':trimscan_path,
                   'settings':settings['set'],
                   'energy':energy,
                   'element':elem_string
                  }

    # Try to load in previous trimbit calibration parameters from files, if they exist
    if autotrim_path:
        try:
            for index in range(0, 16):
                trim_filename = 'MST_autotrim_b01_m01_c{:02d}.dat'.format(index)
                trim_setting = read_trimbit_file(os.path.join(autotrim_path, trim_filename))

                trimkey = 'MST_autotrim_b01_m01_c{:02d}'.format(index)
                calibration[trimkey] = trim_setting
        except:
            logging.warning('Could not load trimbit calibration for: {}'.format(trimscan_path))

    return calibration

def get_included_pixels(excluded_chips=[], key='MST'):
    """
    Get a map of booleans describing which pixels should be included in the calibration routine.
    The argument 'excluded_chips' is an optional array that allows specific chips to be excluded
    by their number (for instance excluded_chips=[2,8] excludes both chips 2 and 8). This routine
    also automatically excludes bad pixels and the artificial software pixels of chip '-1'.
    """
    included_pixels = np.full((M_SIZE_X, M_SIZE_Y), True, dtype=bool)

    for x_index in range(M_SIZE_X):
        for y_index in range(M_SIZE_Y):
            chip_num = pix.get_chip_coords(x_index, y_index)[0]

            if chip_num in excluded_chips:
                included_pixels[x_index, y_index] = False
            elif chip_num == -1:
                included_pixels[x_index, y_index] = False
            elif (x_index, y_index) in bad_pixels[key]:
                included_pixels[x_index, y_index] = False

    return included_pixels

def read_settings_file(filename=None):
    """
    Description
    ===============
        Read pilatus2 settings files. If a setting is multiply defined in the file, the last set
        value will be returned.
    Parameters:
    ===============
        - Filename = (string) Path to a specific "setdacs_b01_m01.dat" file.
    Returns:
    ===============
        - settings = (dict) Pilatus camera settings recorded at the time of calibration data
                     acquisition. This includes the trimbit step voltage, the 16 comparator
                     voltage thresholds, voltage gains, etc.
    """
    settings = {}
    settings['set'] = {}
    settings['prog'] = {}

    for line in open(filename, 'r'):
        line_parts = line.split()

        # Read the detector settings from the file and store in the dictionary, using
        # the setting name as a key.
        if line_parts[0] == 'set':
            settings['set'][line_parts[1]] = float(line_parts[2])

        # Read in the detector "prog" settings. Not sure what this does, it may be residual.
        # Current data does not seem to include any such lines.
        if line_parts[0] == 'prog':
            settings['prog'][line_parts[1]] = float(line_parts[2])

    return settings

def read_trimbit_file(filename=None):
    """
    Description
    ===============
        This is a very specific routne to read the autotrim files for the Pilatus detectors.

        This function accepts a path to the "autotrim_b01_m01_c##.dat" files and reads the
        trimbit settings. These are then stored into a numpy array for easy manipulation. These
        files specify the trimbit settings when data is collected, thus setting the energy range.
    Parameters:
    ===============
        - filename = (string) Path to autotrim file
    Returns:
    ===============
        - (dict) Continains settings information and individual trimbit settings for the
                 chip which the file corresponds to. Trimbit values are indexed by pixel
                 coordinate, i.e. trimfile['trim'][25,102] gives the trimbit setting for
                 the pixel (x,y) = (25,102), where trim_file = read_trimbit_file(...).

        TODO: I think this function can be simplified and made more pythonic. The use of
              a temporary storage array is unlikely to be necessary.
    """
    logging.info('Loading trimbit calibration from {}'.format(filename))

    trim_temp = {}
    trim_temp['x'] = []
    trim_temp['y'] = []
    trim_temp['trim'] = []

    settings = {}

    for line in open(filename, 'r'):
        line_parts = line.split()

        # Read settings from the autotrim file and store with the specified key
        if line_parts[0] == 'set':
            settings[line_parts[1]] = float(line_parts[2])

        # Record the pixel coordinates and associated trimbit setting (from hexadecimal)
        elif line_parts[0] == 'trim':
            trim_temp['x'].append(int(line_parts[1]))
            trim_temp['y'].append(int(line_parts[2]))
            trim_temp['trim'].append(int(line_parts[3], 16))

    # Create an numpy array with the data.
    num_x = max(trim_temp['x']) + 1
    num_y = max(trim_temp['y']) + 1
    trim_values = np.zeros((num_x, num_y), dtype=np.int)

    # Create an array of trimbit setting for a chip, indexed by x and y pixel coordinate
    for index in range(0, len(trim_temp['x'])):
        trim_values[trim_temp['x'][index], trim_temp['y'][index]] = trim_temp['trim'][index]

    return {'settings':settings, 'trim':trim_values}

def get_default_settings(name, refit_scurves=True, refit_energy=True):
    """
    Get the default settings dictionary for a specified trimbit and energy calibration.
    """
    if name == 'MST_2_to_7_keV':
        settings = {'trimscans':
            {
            '2017-06-22_2_to_7_keV_scan_vacuum':
                {
                'Zr': {'start_bit':{2:4}, 'max_start_bit':0, 'exclude_chips':[2], 'refit_scurve': refit_scurves, 'chi_limit': 6.0},
                'Mo': {'start_bit':{2:4}, 'max_start_bit':4, 'exclude_chips':[], 'refit_scurve': refit_scurves, 'chi_limit': 6.0},
                'Ag': {'start_bit':{2:4}, 'max_start_bit':4, 'exclude_chips':[], 'refit_scurve': refit_scurves, 'chi_limit': 6.0},
                'In': {'start_bit':{2:4}, 'max_start_bit':4, 'exclude_chips':[], 'refit_scurve': refit_scurves, 'chi_limit': 6.0},
                'Ti': {'start_bit':4, 'max_start_bit':4, 'exclude_chips':[], 'refit_scurve': refit_scurves, 'chi_limit': 6.0},
                'V':  {'start_bit':4, 'max_start_bit':4, 'exclude_chips':[], 'refit_scurve': refit_scurves, 'chi_limit': 6.0}
                },
            '2017-06-22_4_to_6_keV_scan':
                {
                'Ti': {'start_bit':4, 'max_start_bit':4, 'exclude_chips':[], 'refit_scurve': refit_scurves, 'chi_limit': 6.0},
                'Cr': {'start_bit':4, 'max_start_bit':4, 'exclude_chips':[], 'refit_scurve': refit_scurves, 'chi_limit': 6.0},
                'Fe': {'start_bit':4, 'max_start_bit':4, 'exclude_chips':[], 'refit_scurve': refit_scurves, 'chi_limit': 6.0}
                }
            },
        'refit_energy': refit_energy,
        'calibration_path': '/home/patrick/Mount/data/meSXR/MST/trimscan_data',
        'output_path': '/home/patrick/Mount/data/meSXR/MST/calibration',
        'name': 'MST_2_to_7_keV',
        'device': 'MST'}

    elif name == 'MST_2_to_7_keV_vacuum':
        settings = {'trimscans':
            {
            '2017-06-22_2_to_7_keV_scan_vacuum':
                {
                'Zr': {'start_bit':{2:4}, 'max_start_bit':0, 'exclude_chips':[2], 'refit_scurve': refit_scurves, 'chi_limit': 6.0},
                'Mo': {'start_bit':{2:4}, 'max_start_bit':4, 'exclude_chips':[], 'refit_scurve': refit_scurves, 'chi_limit': 6.0},
                'Ag': {'start_bit':{2:4}, 'max_start_bit':4, 'exclude_chips':[], 'refit_scurve': refit_scurves, 'chi_limit': 6.0},
                'In': {'start_bit':{2:4}, 'max_start_bit':4, 'exclude_chips':[], 'refit_scurve': refit_scurves, 'chi_limit': 6.0},
                'Ti': {'start_bit':4, 'max_start_bit':4, 'exclude_chips':[], 'refit_scurve': refit_scurves, 'chi_limit': 6.0},
                'V':  {'start_bit':4, 'max_start_bit':4, 'exclude_chips':[], 'refit_scurve': refit_scurves, 'chi_limit': 6.0}
                },
            },
        'refit_energy': refit_energy,
        'calibration_path': '/home/patrick/Mount/data/meSXR/MST/trimscan_data',
        'output_path': '/home/patrick/Mount/data/meSXR/MST/calibration',
        'name': 'MST_2_to_7_keV_vacuum',
        'device': 'MST'}

    elif name == 'MST_4_to_12_keV':
        settings = {'trimscans':
            {
            '2017-06-20_4_to_12_keV_quickscan':
                {
                'Cr': {'start_bit':0, 'max_start_bit':0, 'exclude_chips':[], 'refit_scurve': refit_scurves, 'chi_limit': 10.0},
                'Fe': {'start_bit':0, 'max_start_bit':0, 'exclude_chips':[], 'refit_scurve': refit_scurves, 'chi_limit': 10.0},
                'Cu': {'start_bit':5, 'max_start_bit':5, 'exclude_chips':[], 'refit_scurve': refit_scurves, 'chi_limit': 10.0},
                'Ge': {'start_bit':5, 'max_start_bit':10, 'exclude_chips':[], 'refit_scurve': refit_scurves, 'chi_limit': 50.0},
                'Br': {'start_bit':5, 'max_start_bit':10, 'exclude_chips':[], 'refit_scurve': refit_scurves, 'chi_limit': 50.0}
                }
            },
        'refit_energy': refit_energy,
        'calibration_path': '/home/patrick/Mount/data/meSXR/MST/trimscan_data',
        'output_path': '/home/patrick/Mount/data/meSXR/MST/calibration',
        'name': 'MST_4_to_12_keV',
        'device': 'MST'}

    elif name == 'DIIID_4_to_12_keV':
        settings = {'trimscans':
            {
            '2017-06-20_4_to_12_keV_scan':
                {
                'Cr': {'start_bit':0, 'max_start_bit':0, 'exclude_chips':[], 'refit_scurve': refit_scurves, 'chi_limit': 10.0},
                'Fe': {'start_bit':0, 'max_start_bit':0, 'exclude_chips':[], 'refit_scurve': refit_scurves, 'chi_limit': 10.0},
                'Cu': {'start_bit':5, 'max_start_bit':5, 'exclude_chips':[], 'refit_scurve': refit_scurves, 'chi_limit': 10.0},
                'Ge': {'start_bit':5, 'max_start_bit':10, 'exclude_chips':[], 'refit_scurve': refit_scurves, 'chi_limit': 50.0},
                'Br': {'start_bit':5, 'max_start_bit':10, 'exclude_chips':[], 'refit_scurve': refit_scurves, 'chi_limit': 50.0}
                }
            },
        'refit_energy': refit_energy,
        'calibration_path': '/home/patrick/Mount/data/meSXR/DIIID/trimscan_data',
        'output_path': '/home/patrick/Mount/data/meSXR/DIIID/calibration',
        'name': 'DIIID_4_to_12_keV',
        'device': 'DIIID'}

    else:
        print('Specified settings not yet implemented.')
        settings = -1

    return settings
