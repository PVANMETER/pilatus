#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Brief
================
    This file contains utuility functions used by the Pilatus multi-energy calibration script
    to fit configure the Pilatus pixel energy configuration. This also includes generating
    autotrim files to be used by the detector.
Module Contents
================
    1. get_pixel_configuration(config_type=None, energyfit=None)
    2. generate_trimbit_config(configuration, energyfit)
    3. write_autotrim_from_fit(trimfits, calib_path)
    4. write_autotrim_from_image(trim_image, output_path, prefix=None)
    5. write_trimbit_file(trims, chip_num=None, filename=None)
    6. get_chip_coords(image_x, image_y)

To use this file, imclude "import trimfit as trim" or a similar statement in the
top of the desired module.
TODO: Merge autotrim writing functions.

Code originally by Novimir Pablant and Jake Maddox, currently maintained by Patrick VanMeter.
Last updated: 5 Dec. 2017
"""
import logging
import os
from PIL import Image
import numpy as np
import energyfit as enrgy

#Configure the logging system
logging.basicConfig(level=logging.INFO)

# Geometric constants
M_NUM_CHIPS_Y = 2
M_NUM_CHIPS_X = 8
M_NUM_CHIPS = M_NUM_CHIPS_X * M_NUM_CHIPS_Y
M_CHIP_SIZE_Y = 97
M_CHIP_SIZE_X = 60
M_SIZE_Y = 195
M_SIZE_X = 487
M_NUM_TRIM = 64

def configure_detector(config_type, energy_calibration, output_path):
    """
    Configures the detector.
    """
    # 3. Retrive the energy configuration that we want.
    configuration = get_pixel_configuration(config_type)

    # Now generate the trimbit image.
    trimbit_config = generate_trimbit_config(configuration, energy_calibration['energyfits'])
    write_autotrim_from_image(trimbit_config, output_path, prefix=config_type+'_')

    # Save the configuration request.
    filename = os.path.join(output_path, config_type+'_trimbit_request.tif')
    save_tiff_image(trimbit_config, filename=filename, mode='F')

    filename = os.path.join(output_path, config_type+'_energy_threshold_request.tif')
    save_tiff_image(configuration, filename=filename, mode='F')

    # Save the actual configuration after rounding.
    trimbit_final, threshold_final = round_config(energy_calibration['energyfits'], trimbit_config)
    config_uncertainty = threshold_uncertainty(energy_calibration['energyfits'], energy_calibration['energycovs'],
                                               trimbit_config)

    filename = os.path.join(output_path, config_type+'_trimbit.tif')
    save_tiff_image(trimbit_final, filename=filename, mode='I')

    filename = os.path.join(output_path, config_type+'_energy_threshold.tif')
    save_tiff_image(threshold_final, filename=filename, mode='F')

    filename = os.path.join(output_path, config_type+'_energy_uncertainty.tif')
    save_tiff_image(config_uncertainty, filename=filename, mode='F')

    return {'trimbits':trimbit_final, 'threshold':threshold_final,
            'trimbits request':trimbit_config, 'threshold request':configuration,
            'threshold uncertainty':config_uncertainty}

def get_pixel_configuration(config_type=None):
    """
    Description
    ===============
    Return an image with the desired energy calibration on each pixel.

    To start with I will assume that I am using the same value of Vcmp for the
    entire module.
    """
    configuration = np.zeros([M_SIZE_X, M_SIZE_Y])

    if config_type == '4_strips_Mn_K_alpha':
        """
        This configuration is a proof of concept to be used with the Fe-55 source
        at MST. It consists of four equally-spaced bars with different energy thresholds.
        This allows a basic demonstration of both the energy and spatial sensitivity of the
        detector.
        """
        energies = [5.6, 5.8, 6.0, 6.2]
        configuration[:, 0:48] = energies[0]
        configuration[:, 48:96] = energies[1]
        configuration[:, 96:144] = energies[2]
        configuration[:, 144:195] = energies[3]

    elif config_type == '4_to_7_keV_strips':
        dE = (7 - 4)/64.
        for index in range(65):
            configuration[:, index*3:index*3+3] = 4 + index*dE

    elif config_type == '3p5_to_6p5_keV_strips':
        dE = (6.5 - 3.5)/64.
        for index in range(65):
            configuration[:, index*3:index*3+3] = 3.5 + index*dE

    elif config_type == '3p0_to_6p0_1d_metapixels':
        start_E = 3.0
        end_E = 6.0
        num_energies = 8
        dE = (end_E - start_E) / (num_energies-1)
        thresholds = np.arange(start_E, end_E+dE/2, dE)
        num_cols = int(M_CHIP_SIZE_X*M_NUM_CHIPS_X / num_energies)

        # Cycle through pixels, skipping over the boundary columns
        dummy_index = 0
        for x_base in range(num_cols):
            for en_index, thresh in enumerate(thresholds):
                chip_num = get_chip_coords(x_base*num_energies + en_index + dummy_index, 0)[0]
                if chip_num == -1:
                    dummy_index += 1
                configuration[x_base*num_energies + en_index + dummy_index, :] = thresh

    elif config_type == '1p8_to_4p0_8_stripes':
        start_E = 1.8
        end_E = 4.0
        num_energies = 8
        dE = (end_E - start_E) / (num_energies-1)
        thresholds = np.arange(start_E, end_E+dE/2, dE)
        num_cols = int(M_CHIP_SIZE_X*M_NUM_CHIPS_X / num_energies)

        # Cycle through pixels, skipping over the boundary columns
        dummy_index = 0
        for x_base in range(num_cols):
            for en_index, thresh in enumerate(thresholds):
                chip_num = get_chip_coords(x_base*num_energies + en_index + dummy_index, 0)[0]
                if chip_num == -1:
                    dummy_index += 1
                configuration[x_base*num_energies + en_index + dummy_index, :] = thresh

    elif config_type == '4p0_to_10p0_8_stripes':
        start_E = 4.0
        end_E = 10.0
        num_energies = 8
        dE = (end_E - start_E) / (num_energies-1)
        thresholds = np.arange(start_E, end_E+dE/2, dE)
        num_cols = int(M_CHIP_SIZE_X*M_NUM_CHIPS_X / num_energies)

        # Cycle through pixels, skipping over the boundary columns
        dummy_index = 0
        for x_base in range(num_cols):
            for en_index, thresh in enumerate(thresholds):
                chip_num = get_chip_coords(x_base*num_energies + en_index + dummy_index, 0)[0]
                if chip_num == -1:
                    dummy_index += 1
                configuration[x_base*num_energies + en_index + dummy_index, :] = thresh

    elif config_type == 'low_E_1d_metapixels':
        thresholds = np.array([1.8, 2.0, 3.0, 4.0])
        num_energies = len(thresholds)
        num_cols = int(M_CHIP_SIZE_X*M_NUM_CHIPS_X / num_energies)

        # Cycle through pixels, skipping over the boundary columns
        dummy_index = 0
        for x_base in range(num_cols):
            for en_index, thresh in enumerate(thresholds):
                chip_num = get_chip_coords(x_base*num_energies + en_index + dummy_index, 0)[0]
                if chip_num == -1:
                    dummy_index += 1
                configuration[x_base*num_energies + en_index + dummy_index, :] = thresh

    elif config_type == 'metapixel':
        # Define a meta pixel configuration.
        # for this configuration I will split the detector into 3x3 meta pixels
        # where each pixel within the meta pixel has a different threshold energy.

        # This will create 9 pixels with the following energies:
        # 4, 5, 6, 7, 8, 9, 10, 11, 12 keV

        # For this configuration I will ignore the software pixels so that
        # it is easy to constuct and compare images taken with each
        # energy threshold.
        for pixel_x in range(0, M_SIZE_X):
            for pixel_y in range(0, M_SIZE_Y):
                energy = pixel_x%3*3 + pixel_y%3 + 4
                configuration[pixel_x, pixel_y] = energy

    elif config_type == 'y_strips_fine':
        # Here I will create a configuration where each column, in the poloidal
        # direction, has a different energy threshold.  I will use very finely
        # spaced tresholds.
        #
        # For this configuration I will ignore the software pixels so that
        # it is easy to constuct and compare images taken with each
        # energy threshold.
        for pixel_x in range(0, M_SIZE_X):
            for pixel_y in range(0, M_SIZE_Y):
                energy = pixel_x%13 + 4
                configuration[pixel_x, pixel_y] = energy

    elif config_type == 'x_strips_fine':
        # Here I will create a configuration where each row, in the toroidal
        # direction, has a different energy threshold.  I will use very finely
        # spaced tresholds.
        #
        # I am trying to use a threshold step that is smaller than the
        # energy per trimbit at the lower energies.  This means that the spread
        # in actuall energy thresholds is fairly large at the lower energies.
        #
        # Here I skip any software pixels.

        for ii_chip_x in range(0, M_NUM_CHIPS_X):
            for ii_chip_y in range(0, M_NUM_CHIPS_Y):

                # Find the starting pixel for this chip (skipping any software pixels).
                x_start = M_CHIP_SIZE_X*ii_chip_x + ii_chip_x
                y_start = M_CHIP_SIZE_Y*ii_chip_y + ii_chip_y

                # Now loop over the pixels in this chip.
                for ii_pixel_x in range(0, M_CHIP_SIZE_X):
                    for ii_pixel_y in range(0, M_CHIP_SIZE_Y):

                        energies = np.arange(4.0, 16.0, 0.5)

                        configuration[x_start + ii_pixel_x, y_start + ii_pixel_y] = \
                            energies[ii_pixel_y%energies.size]

    elif config_type == 'x_strips_20120629':
        # Here I will create a configuration where each row, in the toroidal
        # direction, has a different energy threshold.  I will use very finely
        # spaced tresholds.
        #
        # In this configuration I will use more columns for the higher energy
        # thresholds, and fewer for the higher energy thresholds inorder to
        # partially balace photon statistics between the various energy bins.
        #
        #
        # I am trying to use a threshold step that is smaller than the
        # energy per trimbit at the lower energies.  This means that the spread
        # in actuall energy thresholds is fairly large at the lower energies.
        #
        # Here I skip any software pixels.

        energies = np.linspace(16.0, 4.0, 25)

        # Generated using a modified version of K.Hill's PHA_OPT script.
        # This determines the number of rows from SQRT( INTEGRATE(exp(-Es/Te)/sqrt(Te)) ).
        # The SQRT is there becasue we can't accomodate the three orders of magnitude in
        # that we would need to give each energy the same total intensity.
        ncols = np.array([25, 22, 18, 16, 13, 11, 9, 8, 7, 6, 5, 4, 4, 3, 2, 2, 2, 2, 1,
                          1, 1, 1, 1, 1, 1])


        # First set all of the energies to the lowest threshold.
        for pixel_x in range(0, M_SIZE_X):
            for pixel_y in range(0, M_SIZE_Y):
                configuration[pixel_x, pixel_y] = energies[-1]

        # Now loop over all the energies.
        start_column = 0
        for ii, energy in enumerate(energies):
            for jj in range(0, ncols[ii]):
                for pixel_x in range(0, M_SIZE_X):
                    pixel_y = start_column + jj
                    configuration[pixel_x, pixel_y] = energy

            start_column += ncols[ii]

    elif config_type == 'y_strips':
        pass

    elif config_type.split('_')[0:2] == ['single', 'energy']:
        """
        Catch-all to correctly deal with all single-energy configurations up to one decial
        point. Format like >> config_type = 'single_energy_2p6', 'single_energy_3p0', etc.
        """
        threshold_string = config_type.split('_')[-1]
        digits = threshold_string.split('p')
        configuration[:, :] = float(digits[0]) + 0.1*float(digits[1])
        return configuration

    else:
        raise ValueError('Requested configuration not found. Please revise input.')

    return configuration

def generate_trimbit_config(configuration, energyfits):
    """
    Description
    ===============
    Generate a image of trimbit settings that match the given energy configuration.
    To do this we use the fit of the trimbit-energy dependence.
    Need to come up with an approximate solution for pixels without a valid calibration.
    """
    trimbit_config = np.zeros(configuration.shape)

    for x_index in range(M_SIZE_X):
        for y_index in range(M_SIZE_Y):
            # Skip this pixel if the configuration is set to zero.
            # This should only be the case at the software pixels.
            # Also want to address pixels with failed energy fits
            if configuration[x_index, y_index] != 0.0:
                # Determine the trimbit from our energy fit.
                trimbit_config[x_index, y_index] = enrgy.trimbit_from_energy(energyfits[x_index, y_index, :],
                                                                       configuration[x_index, y_index])

    return trimbit_config

def round_config(energyfits, trimbit_config):
    """
    Description
    ===============
    Rounds the trimbit values to integers, and calculates the
    """
    trimbit_config_rounded = np.round(trimbit_config)
    trimbit_config_rounded[np.where(trimbit_config_rounded < 0)] = 0
    trimbit_config_rounded[np.where(trimbit_config_rounded > 63)] = 63

    config_rounded = np.zeros((M_SIZE_X, M_SIZE_Y))
    for ii_x in range(M_SIZE_X):
        for ii_y in range(M_SIZE_Y):
            config_rounded[ii_x, ii_y] = enrgy.energy_from_trimbit(energyfits[ii_x, ii_y, :],
                                                                   trimbit_config_rounded[ii_x, ii_y])

    return trimbit_config_rounded, config_rounded

def threshold_uncertainty(energyfits, energycov, trimbit_config):
    """
    Description
    ===============
    Determines the uncertainty in each pixel threshold energy given the fits and the trimbit configuration.
    """
    config_uncertainty = np.zeros((M_SIZE_X, M_SIZE_Y))
    for x_index in range(M_SIZE_X):
        for y_index in range(M_SIZE_Y):
            config_uncertainty[x_index, y_index] = enrgy.energy_uncertainty(energyfits[x_index, y_index, :],
                                                                            energycov[x_index, y_index, :, :],
                                                                            trimbit_config[x_index, y_index])
                                
    return config_uncertainty

def save_tiff_image(image, filename=None, mode='F'):
    """
    Description
    ===============
        Original note from Pablant:
            There is no way to save a tiff file in integer mode from scipy without scaling.
            This copies parts of scipy.misc.pilutil.toimage without the scaling.

        This function takes in an array of data and converts it into an image which is then
        saved as an image file. The filename specifies the filetype (use tiff).

    NOTE: Changed Image.fromstring(...) to Image.frombytes(...) as the former has been
            deprecated in PIL. Unsure if the size argument is being used correctly.
    Parameters:
    ===============
        - image = (int[SIZE_X, SIZE_Y]) 2D array containing the desired pixel configuration.
        - filename = (string) Specify the name of the output file.
        - mode = (string) Save data as floats ('F') or as 32-bin ints ('I')
    Returns:
    ===============
        - None
    """
    # Convert the image to a standard numpy array and get the dimensions
    data = np.asarray(image)
#    dim = tuple(reversed(data.shape))   # Transposed shape. Maybe deprecated?

    if mode == 'F':
        data32 = data.astype(np.float32)
        image = Image.frombytes(mode, data32.shape, data32.transpose().tostring())
    if mode == 'I':
        data32 = data.astype(np.uint32)
        image = Image.frombytes(mode, data32.shape, data32.transpose().tostring())

    image.save(filename)
    #tifffile.imsave(filename, image)
    logging.info('Image saved to file: {}'.format(filename))

def write_autotrim_from_fit(trimfits, calib_path):
    """
    Description
    ===============
    Take my trimfits and write out a set of autotrim files.
    """
    trims = trimfits[:, :, 0]
    sigma = trimfits[:, :, 1]
    write_autotrim_from_image(trims, calib_path, prefix='MST_')

def write_autotrim_from_image(trim_image, output_path, prefix=None):
    """
    Description
    ===============
    Take my trimfits and write out a set of autotrim files.
    """
    if not prefix:
        raise Exception('A prefix must be given so that original files are not overwritten.')

    # Loop though all the pixels in the image map to the trim settings for each of the chips.
    chip_trims = np.zeros((M_CHIP_SIZE_X, M_CHIP_SIZE_Y, M_NUM_CHIPS))

    for image_x in range(0, M_SIZE_X):
        for image_y in range(0, M_SIZE_Y):
            chip_n, chip_x, chip_y = get_chip_coords(image_x, image_y)

            # Ignore any software pixels (which do not really exist on a chip).
            if chip_n >= 0:
                chip_trims[chip_x, chip_y, chip_n] = trim_image[image_x, image_y]

    # Loop over the chips and save the trimbit data.
    for chip_n in range(0, M_NUM_CHIPS):

        # generate a filename.
        filename = os.path.join(output_path, '{}autotrim_b01_m01_c{:02d}.dat'.format(prefix, chip_n))

        # I am currenly defining x and y backwards from what dectris uses.
        write_trimbit_file(chip_trims[:, :, chip_n], chip_n, filename)

def write_trimbit_file(trims, chip_num=None, filename=None):
    """
    Description
    ===============
        Write the given trim settings into an autotrim file.
    Parameters:
    ===============
        - trims = ()
        - chip_num = ()
        filename = ()
    Returns:
    ===============
        - None
    """
    logging.info('Writing trimbit calibration to {}'.format(filename))
    fileobj = open(filename, 'w')

    # Write out a comment
    fileobj.write("# Pilatus AUTOTRIM file generated by claibration_MST code. \n")
    fileobj.write('set B01_M01_CHSEL{} 1\n'.format(chip_num))

    """
    Note from Pablant:
        I am going to write these files by just looping through x and y on each chip.
        This will no be in the same order as the file written by dectris, since there
        pixels are looped though x and y on the module and written into the chip
        files in what ever order that turned out to be.
    """
    for ii_x in range(trims.shape[0]):
        for ii_y in range(0, trims.shape[1]):
            trim_rounded = round(trims[ii_x, ii_y])
            if np.isnan(trim_rounded):
                logging.warning('NaN found in trim values.')
                trim_rounded = 0

            # Check if the trimvalue is in bounds.
            if trim_rounded < 0:
                trim_rounded = 0
            if trim_rounded > 63:
                trim_rounded = 63

            # Write to file.  Round trimbits to the nearest integer values.
            fileobj.write('trim {} {} {}\n'.format(ii_x, ii_y, hex(int(trim_rounded))))

    fileobj.write('settrims\n')
    fileobj.close()

def get_chip_coords(image_x, image_y):
    """
    Description
    ===============
        Original note from Pablant:
            This is copied from pix_add in utils.c for the p2det detector. Given an x and y
            location on the detector, this will return the appropriate chip number and the x
            and y location on that given chip.

        This function takes coordinates in the broader "image" (detector) reference frame and
        determines chat chip this point falls on as well as its local x,y coordinates in the
        chip reference frame.
    Parameters:
    ===============
        - image_x = (int) X-coordinate of a point on the overall detector image
        - image_y = (int) Y-coordinate of a point on the overall detector image
    Returns:
    ===============
        - chip_num = (int) The chip number on which the point (image_x, image_y) lies
        - chip_x = (int) The x-coordinate of the point in the frame of chip_num
        - chip_y = (int) The y-coordinate of the point in the frame of chip_num
    """
    if image_y < M_SIZE_Y/2:
        chip_num = image_x/(M_CHIP_SIZE_X + 1)
        chip_x = (M_CHIP_SIZE_X+1)*(chip_num+1) - image_x - 2
        chip_y = image_y

        if chip_x < 0:
            chip_num = -1
    elif image_y == M_SIZE_Y/2:
        chip_num = -1

    else:
        chip_num = M_NUM_CHIPS/2 + image_x/(M_CHIP_SIZE_X + 1)
        chip_x = image_x % (M_CHIP_SIZE_X+1)
        chip_y = M_SIZE_Y - image_y - 1

        if chip_x >= M_CHIP_SIZE_X:
            chip_num = -1

    # Check if this is a valid chip.
    if chip_num < 0:
        chip_y = -1
        chip_x = -1

    return chip_num, chip_x, chip_y
