#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Brief
================
    This file contains utuility functions used by the Pilatus multi-energy calibration script
    to fit scaling of energy resolution to trimbit setting, based on the S-curve calibration.

Module Contents
================
    1. trimbit_from_energy(coeff, x=None)
    2. trimbit_from_energy_residual(coeff, fjac=None, x=None, y=None, err=None)
    3. fit_energy_dependence_single(energy, trimbit, trims_error=0)
    4. fit_energy_dependence(calibration, index=None)
    5. energy_from_trimbit(energyfit, trimbit)
    6. energy_per_trimbit(energyfit, trimbit)
    7. trimbits_per_energy(energyfit, energy)
    8. get_energy_resolution(calibration, pixel=[30, 30])
    9. plot_energy_dependence(calibration, pixel=[30, 30])

To use this file, imclude "import energyfit as enrgy" or a similar statement in the
top of the desired module.
TODO: Move from mpfit to scipy.optimize.curvefit

Code originally by Novimir Pablant and Jake Maddox, currently maintained by Patrick VanMeter.
Last updated: 5 Dec. 2017
"""
import logging
import numpy as np
from scipy.optimize import curve_fit
import calibration as calib

#Configure the logging system
logging.basicConfig(level=logging.INFO)

# Geometric constants
M_SIZE_Y = 195
M_SIZE_X = 487
M_NUM_TRIM = 64

def fit_energy_dependence(calibrations, key='MST'):
    """
    Description
    ===============
    Find the dependence of the trimbit value with x-ray energy for all pixels.
    """
    logging.info('Starting trimbit energy fits.')

    # Load in the calibration all line energies and create storage for the fits
    energyfit = np.zeros([M_SIZE_X, M_SIZE_Y, 3])
    energycov = np.zeros([M_SIZE_X, M_SIZE_Y, 3, 3])
    energyfails = np.zeros([M_SIZE_X, M_SIZE_Y], dtype=np.int8)

    # Get the list of pixels included, leaving out bad pixels and chip = -1
    included_pixels = calib.get_included_pixels(key=key)

    # Loop over all pixels in the detector
    for x_index in range(M_SIZE_X):
        for y_index in range(M_SIZE_Y):
            # Manually fail for excluded pixels
            if not included_pixels[x_index, y_index]:
                energyfails[x_index, y_index] = 1

            else:
                # Load in the data from the trimscan, including only desired/successful fits
                data = []
                energy = []
                sigma = []

                for cal in calibrations:
                    if not cal['trimfails'][x_index, y_index]:
                        data.append(cal['trimfits'][x_index, y_index, 0])
                        energy.append(cal['energy'])
                        sigma.append(np.sqrt(cal['trimcovs'][x_index, y_index, 0, 0]))

                data = np.array(data)
                energy = np.array(energy)
                sigma = np.array(sigma)

                # Fit the trimbit vs 50% cutoff energy data to a parameterized curve
                try:
                    fit_param, fit_cov = fit_pixel_energy(energy, data, sigma)
                    energyfit[x_index, y_index, :] = fit_param
                    energycov[x_index, y_index, :, :] = fit_cov

                except Exception:
                    print('Energy fit failed for pixel (' + str(x_index) + ', ' + str(y_index) + ')')
                    energyfails[x_index, y_index] = 1

                except KeyboardInterrupt:
                    # Manually stop the whole program
                    print('Exiting calibration. Data has not been saved.')
                    raise SystemExit(0)

            # Counter to keep up with fitting progress
            count = (x_index)*(M_SIZE_Y) + y_index
            if count%1000 == 0:
                print('{:4.0f}% done'.format(float(count)/(M_SIZE_X*M_SIZE_Y)*100.))

    logging.info('Done with trimbit energy fits.')

    return energyfit, energycov, energyfails

def fit_pixel_energy(energy, trimbit, trimerr, nonlinear=True):
    """
    Description
    ===============
    Find the dependence of the trimbit value with x-ray energy for a single pixel.

    Updated to use polyfit instead of the nonlinear optimizer. Since this is a linear fit
    this should be much faster and less likely to fail. The old method still lives behind
    the "nonlinear" keyword, but should only be used if there are insufficient data points
    to estimate the covariance matrix via standard linear methods.
    """
    # If not enough data points are provided (must exceed order + 2), the nonlinear fit
    # must be used.
    if len(energy) <= 5:
        nonlinear = True

    if nonlinear:
        # Use a nonlinear fitting algorithm - previous behavior
        # Initial guess.
        p0 = [np.mean(trimbit), 0, 0]

        # Perform the nonlinear fit
        popt, pcov = curve_fit(trimbit_from_energy_sp, energy, trimbit,
                               sigma=trimerr, absolute_sigma=True)

    else:
        """
        Use a linear fitting algorithm - current default
        The fit is weighted with the supplied 1σ uncertainties - specified to be 1/sigma, not 1/sigma^2
        for Gaussian uncertainties (see numpy.polyfit documentation).

        Note that a minimum of 6 data points are required in order to obtain an estimate of the covariance
        matrix for a fit of this order. If fewer points are available, default to old_fit.
        """
        trim_weights = 1/trimerr
        popt, pcov = np.polyfit(energy, trimbit, w=trim_weights, deg=2, cov=True)

        # Convert coefficient indices to conventions used in this module
        popt = np.flipud(popt)
        pcov = np.flipud(np.fliplr(pcov))

    return popt, pcov

def trimbit_from_energy(coeff, x=None):
    """
    Description
    ===============
    Returns trimbit setting as a function of energy for a given fit.
    """
    return coeff[0] + coeff[1]*x + coeff[2]*x**2

def trimbit_from_energy_sp(x, fit_A, fit_B, fit_C):
    """
    For use with scipy
    """
    return fit_A + fit_B*x + fit_C*x**2

def energy_from_trimbit(energyfit, trimbit):
    """
    Description
    ===============
    Given a trimbit value, return the associated 50% threshold energy.
    """
    energy = ((-1*energyfit[1] +
               np.sqrt(energyfit[1]**2 - 4*energyfit[2]*(energyfit[0] - trimbit)))/(2*energyfit[2]))

    return energy

def energy_per_trimbit(energyfit, trimbit):
    """
    Description
    ===============
    Return the slope of energy vs trimbit value, dE/dT.
    """
    energy = energy_from_trimbit(energyfit, trimbit)
    dEdT = 1./(2*energyfit[2]*energy + energyfit[1])

    return dEdT

def trimbits_per_energy(energyfit, energy):
    """
    Description
    ===============
    Return the slope of trimbit vs energy, dT/dE = (dE/dT)^-1
    """
    return 2*energyfit[2]*energy + energyfit[1]

def get_energy_resolution(calibration, energyfit, pixel=[30, 30]):
    """
    Description
    ===============
    Get the energy resolution of a specific pixel.
    Not used in calibration.
    """
    resolution = [energy_per_trimbit(energyfit[pixel[0], pixel[1], :],
                                     c['trimfits'][pixel[0], pixel[1], 0])*
                  c['trimfits'][pixel[0], pixel[1], 1]*2*np.sqrt(2*np.log(2))
                  for c in calibration]

    print(['{:5.3f}'.format(x) for x in resolution])

    resolution2 = []
    for current_calib in calibration:
        hwhm = current_calib['trimfits'][pixel[0], pixel[1], 1]*np.sqrt(2*np.log(2))
        t0 = current_calib['trimfits'][pixel[0], pixel[1], 0]
        trimbit_high = t0 + hwhm
        trimbit_low = t0 - hwhm

        energy_high = energy_from_trimbit(energyfit[pixel[0], pixel[1], :], trimbit_high)
        energy_low = energy_from_trimbit(energyfit[pixel[0], pixel[1], :], trimbit_low)

        resolution2.append(energy_high - energy_low)

    print(['{:5.3f}'.format(x) for x in resolution2])

    return resolution

def trimbit_uncertainty(en_cov, energy):
    """
    Description
    ===============
    Returns the uncertainty associated with the trimbit setting, given a desired
    threshold energy.
    """
    sigma_var = lambda x: en_cov[0, 0] + en_cov[1, 1]*(x**2) + en_cov[2, 2]*(x**4)
    sigma_cov = lambda x: 2*en_cov[0, 1]*x + 2*en_cov[0, 2]*(x**2) + 2*en_cov[1, 2]*(x**3)

    return np.sqrt(sigma_var(energy) + sigma_cov(energy))

def energy_uncertainty(en_params, en_cov, trimbit):
    """
    Returns the uncertainty associated with the energy threshold for a sepcified trimbit
    setting.
    """
    energy = energy_from_trimbit(en_params, trimbit)
    sigma_E = energy_per_trimbit(en_params, trimbit)*trimbit_uncertainty(en_cov, energy)

    return sigma_E

def chi_sq_map(trim_calibrations, en_calibration, quiet=False):
    """
    """
    print('Calculating energyfit chi-squared map.')
    chi_map = np.zeros([M_SIZE_X, M_SIZE_Y])
    dof_map = np.zeros([M_SIZE_X, M_SIZE_Y])
    red_map = np.zeros([M_SIZE_X, M_SIZE_Y])

    for x_index in range(M_SIZE_X):
        for y_index in range(M_SIZE_Y):
            # Mark failed fits for easy identification
            if not en_calibration['energyfails'][x_index, y_index]:
                params = en_calibration['energyfits'][x_index, y_index, :]

                # Only include data points included in the fits
                data = []
                energy = []
                sigma = []
                for cal in trim_calibrations:
                    if not cal['trimfails'][x_index, y_index]:
                        data.append(cal['trimfits'][x_index, y_index, 0])
                        energy.append(cal['energy'])
                        sigma.append(np.sqrt(cal['trimcovs'][x_index, y_index, 0, 0]))

                data = np.array(data)
                energy = np.array(energy)
                sigma = np.array(sigma)

                chi, dof = chi_sq(params, energy, data, sigma)
                chi_map[x_index, y_index] = chi
                dof_map[x_index, y_index] = dof
                red_map[x_index, y_index] = chi / dof
            else:
                chi_map[x_index, y_index] = -1
                dof_map[x_index, y_index] = -1
                red_map[x_index, y_index] = -1

            # Counter to keep up with fitting progress
            if not quiet:
                count = (x_index)*(M_SIZE_Y) + y_index
                if count%1000 == 0:
                    print('{:4.0f}% done'.format(float(count)/(M_SIZE_X*M_SIZE_Y)*100.))

    # Make a flattened array for easy histogramming, with bad fits removed
    flat_chi = chi_map.flatten()
    flat_chi = np.delete(flat_chi, np.where(flat_chi == -1))
    flat_red = red_map.flatten()
    flat_red = np.delete(flat_red, np.where(flat_red == -1))

    stat_maps = {'chi-squared': chi_map,
                 'reduced chi-squared': red_map,
                 'dof': dof_map,
                 'chi-squared list': flat_chi,
                 'reduced chi-squared list': flat_red}

    return stat_maps

def chi_sq(params, energy, data, sigma):
    """
    Description
    ===============
        Computes the chi-squared statistic for the cutoff energy fit for a single pixel. The
        uncertainty in the the data must be separately supplied.
    Parameters:
    ===============
        - params = (array) The best-fit parameters for the given fit.
        - energy = (array) The array of cutoff energies corresponding to the supplied trimbit
            data points. The independent variable (x) of the data set.
        - data = (array) The trimbit correspoonding to a 50% given cutoff energy. The
            dependent variable (y) of the data set.
    Returns:
    ===============
        - chi = (float) The caluclated chi-squared statistic for the fit (not reduced).
        - dof = (int) The total degrees of freedom for the fit.
    """
    chi = np.sum((trimbit_from_energy(params, energy) - data)**2 / sigma**2)
    dof = len(data) - len(params)

    return chi, dof

#------------------------------------------------------------------------------------------
def fit_energy_dependence_old(calibrations, index=None):
    """
    Description
    ===============
    Find the dependence of the trimbit value with x-ray energy for all pixels.
    This deprecated implementation has been kept for reference and will eventually
    be removed.
    """
    logging.info('Starting trimbit energy fits.')

    # Load in the calibration line energies and create storage for the fits
    energy = np.array([x['energy'] for x in calibrations])
    energyfit = np.empty([M_SIZE_X, M_SIZE_Y, 3])
    energyerr = np.empty([M_SIZE_X, M_SIZE_Y, 3])

    # Determine number of line energies, if not specifed
    if not index:
        index = range(energy.size)

    # Loop over all pixels in the detector
    for ii_x in range(M_SIZE_X):
        for ii_y in range(M_SIZE_Y):
            # Check the S-Curves.
            # In order to get a good fit to the s-curve I need to be able to see
            # the flattening on the both the low and high ends of the s-curve.
            #
            # This is a crude check to make sure that the s-curve is fittable.
            # it does not guarantee that I got a good fit though.
            index_good = []
            for ii_index in index:
                trimbit_scan = calibrations[ii_index]['trimscan']

                # make sure that the low end is 10% or less of the high end.
                # This ensures that the right hand side of the curve is not
                # cutoff.
                if np.sum(trimbit_scan[ii_x, ii_y, -4:-1])/np.sum(trimbit_scan[ii_x, ii_y, 3:6]) < 0.1:
                    index_good.append(ii_index)

            # Extract the inflection points - the trimbits corresponding to a 50% cutoff at the
            # calibration line energy
            trimbit = np.array([x['trimfits'][ii_x, ii_y, 0] for x in calibrations])
            trimerr = np.array([x['trimerrs'][ii_x, ii_y, 0] for x in calibrations])

            # Fit the trimbit vs E_c data to a parameterized curve - removed [index_good] from both
            fit_param, fit_cov = fit_pixel_energy(energy, trimbit, trimerr)
            energyfit[ii_x, ii_y, :] = fit_param
            energyerr[ii_x, ii_y, :] = np.sqrt(np.diagonal(fit_cov))

            # Counter to keep up with fitting progress
            count = (ii_x)*(M_SIZE_Y) + ii_y
            if count%1000 == 0:
                print('{:4.0f}% done'.format(float(count)/(M_SIZE_X*M_SIZE_Y)*100.))

    logging.info('Done with trimbit energy fits.')

    return energyfit, energyerr