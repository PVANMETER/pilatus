
#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
A
"""

def htpdMakeSCurvePlots(calibration, energyParams, pixel=[30, 30], normalize=True):
    """
    Make a Plot of the S-Curve fits from the trimbit scan for Novimir's HTPD paper.
    TODO: Left in as an example, will likely move to a separate file later.
    """
    ## Create a new matplotlib figure.
    figure()

    trimbit = np.array(range(0, M_NUM_TRIM))
    energy = energyFromTrimbit(energyParams, trimbit)
    trims = np.zeros(np.size(calibration))
    trimsError = np.zeros(np.size(calibration))
    heights = np.zeros(np.size(calibration))
    i = 0
    print(energy)

    for current_calib in calibration:
        ii_x = pixel[0]
        ii_y = pixel[1]
        startBit = 2

        trimbit_scan = current_calib['trimscan']
        index = (np.abs(trimbit_scan[ii_x, ii_y, startBit:-1]-np.max(trimbit_scan[ii_x, ii_y, startBit:-1])/1.5)).argmin()

        y = trimbit_scan[pixel[0], pixel[1], :].copy()
        yfit = pilatusSCurve(current_calib['trimfits'][pixel[0], pixel[1], :], trimbit)
        yfit = fitTrimbitScanSingle(trimbit[startBit:-1], y[startBit:-1])[1]

        if normalize:
            # Normalize to the detector responce when the threshold energy is set to 50%
            # of the energy of the incident x-rays.
            # Don't include the background though when calculating the 50% responce.
            fitParams = current_calib['trimfits'][pixel[0], pixel[1], :]
            fitParams = fitTrimbitScanSingle(trimbit[startBit:-1], y[startBit:-1])[0]
            fitErrors = fitTrimbitScanSingle(trimbit[startBit:-1], y[startBit:-1])[2]
            trimbit_50 = fitParams[0]
            minConstant = fitParams[4]
            fitParams[4] = 0
            print(fitParams)

            if np.size(fitParams) > 7:
                yfit = pilatusDoubleSCurve(fitParams, trimbit)
            else:
                yfit = pilatusSCurve(fitParams, trimbit)

            y = y - np.min(y) + np.min(yfit)

            if trimbit_50 < fitParams[1]*2:
                trimbit_50 = fitParams[1]*2

            y = y/(yfit[trimbit_50-fitParams[1]*2])
            trims[i] = current_calib['energy']
            yfit = yfit/(yfit[fitParams[0]-fitParams[1]*2])
            trimsError[i] = fitErrors[0] * energyPerTrimbit(energyParams, fitParams[0])
            heights[i] = 1./2
            i += 1

        plot(energy, y, color='black')
        plot(energy, yfit, color='red')
        axvline(current_calib['energy'], linestyle='--')

    ax1 = subplot(111)
    ax2 = ax1.twiny()

    errorbar(trims, heights, xerr=trimsError, fmt='.')
    ylim(-0, 1.2)
    ax1.set_xlim(np.min(energy), np.max(energy))
    ax1.set_xlim(np.min(energy), np.max(energy))
    ax1.set_xlabel('Photon Energy Threshold (keV)', fontsize=14, fontweight='bold')
    ax2.set_xlim(ax1.get_xlim())
    ax2.set_xticks(energy[::6])
    ax2.set_xticklabels(trimbit[::6])
    ax2.set_xlabel('Trimbit (6 Bit)', fontsize=14, fontweight='bold')
    ax1.set_ylabel('Normalized Photon Counts', fontsize=14, fontweight='bold')
    fontsize = 14
    ax = gca()

    for tick in ax1.xaxis.get_major_ticks():
        tick.label1.set_fontsize(fontsize)
        tick.label1.set_fontweight('bold')
    for tick in ax2.xaxis.get_major_ticks():
        tick.label2.set_fontsize(fontsize)
        tick.label2.set_fontweight('bold')
    for tick in ax1.yaxis.get_major_ticks():
        tick.label1.set_fontsize(fontsize)
        tick.label1.set_fontweight('bold')

    plt.savefig('VacS-CurvesTrimIn2.eps', transparent=True)

    show()