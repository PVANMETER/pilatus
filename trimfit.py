#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Brief
================
    This file contains utuility functions used by the Pilatus multi-energy calibration script
    to fit the calibration data to an S-curve. This forms the basis of the trimbit-enrgy mapping.
Module Contents
================
    1. read_trimbit_scan(calib_path)
    1. fit_trimbit_scan_single(trimbit, trimbit_scan, double=False)
    2. fit_trimbit_scan(trimbit_scan)
    3. s_curve_residual(p, fjac=None, x=None, y=None, double=False)
    4. s_curve(p, x=None):
    5. double_s_curve(p, x=None)

To use this file, imclude "import trimfit as trim" or a similar statement in the
top of the desired module.
TODO: Move from mpfit to scipy.optimize.curvefit

Code originally by Novimir Pablant and Jake Maddox, currently maintained by Patrick VanMeter.
Last updated: 5 Dec. 2017
"""
from os import path
import numpy as np
from scipy.optimize import curve_fit
from scipy.special import erf
import tifffile
from mirutil.mirmpfit import mpfit
import configuration as pix
import calibration as cal

# Geometric constants
M_SIZE_Y = 195
M_SIZE_X = 487
M_NUM_TRIM = 64
M_NUM_CHIPS_Y = 2
M_NUM_CHIPS_X = 8
M_NUM_CHIPS = M_NUM_CHIPS_X * M_NUM_CHIPS_Y

def load_calibration_data(calib_path):
    """
    Description
    ===============
        Reads in the calibration .tiff files and returns an image indexed by pixel location
        and trimbit setting. These files are photon counts for each pixel with the detector
        exposed to a given emission line, with this measurement repeated for each trimbit
        setting.

        The calibrate_detector routine stores the output of this code under the key "trimscan"
        in the calibration dictionary. This terminology is used throughout the code.
    Parameters:
    ===============
        - calib_path = (string) Path to the folder containing the calibration images.
    Returns:
    ===============
        - images = (int[x_index, y_index, trimbit]) Array containing one calibration
                    image for each of the 64 possible trimbit settings.
    """
    # Read in the params_b01_m01.txt file - left in for future expansion
    with open(path.join(calib_path, 'config.txt'), 'r') as f:
        params = f.readlines()

    # Container for the calibration data
    images = np.empty([M_SIZE_X, M_SIZE_Y, M_NUM_TRIM])

    # Load in the data one trimbit at a time
    for i in range(0, M_NUM_TRIM):
        try:
            filename = path.join(calib_path, 'scan_image_{:03d}.tif'.format(i))
            images[:, :, 63 - i] = tifffile.imread(filename).transpose()
        except:
            # Also try 5-digit labeling, the camserver default
            filename = path.join(calib_path, 'scan_image_{:05d}.tif'.format(i))
            images[:, :, 63 - i] = tifffile.imread(filename).transpose()

    return images

def fit_trimbit_scan(calibration_data, start_bit={}, max_start_bit=4, quiet=False, chi_limit=6.0, key='MST'):
    """
    Description
    ===============
        Perform S-curve fits for all of data in the trimbit calibration scan. See "fit_pixel"
        for a description of the individual fitting process.
    Parameters:
    ===============
        - calib_data = (array of ints) Array contianing the calibration trimscan data for each pixel,
            indexed [x_index, y_index, trimbit].
        - start_bit = (list of ints) Trimbit corresponding to the first bit to include in the
            fitting routine. This is a dictionary indexed by chip (including -1). By
            default start_bit[2] = 4 and for all other values start_bit[i] = 0. Any chip's starting
            bit value not supplied by the user reverts to these defaults. You can also
            pass a single integer value, which will set the start_bit for all chips.
        - max_start_bit = (int) This is the maximum value the starting trimbit can take. This is
            useful because if the fit is not good the algorithm will automatically try the fit
            again with a higher start_bit value. Some calibration elements (i.e. Zr) will not
            fit will without the lowest-end trimbits, so this is a way to force those to fail
            rather than attempt a another fit.
        - quiet = (boolean) Set to True to suppress the progress counter output.
    Returns:
    ===============
        - trimfits = (M_SIZE_X*M_SIZE_Y*6 array) Array containing the best-fit parameters for
            the S-curve, indexed by pixel.
        - trimcov = (M_SIZE_X*M_SIZE_Y*6*6 array) Array containing the full 6x6 covariance
            matrix estimated by the fitting routine, indexed by pixel.
    """
    # Containers for storing iterative results
    trimbit = np.array(range(0, M_NUM_TRIM))
    trimfits = np.zeros([M_SIZE_X, M_SIZE_Y, 6])
    trimcov = np.zeros([M_SIZE_X, M_SIZE_Y, 6, 6])
    trimfails = np.zeros([M_SIZE_X, M_SIZE_Y], dtype=np.int8)
    trimstarts = np.zeros([M_SIZE_X, M_SIZE_Y], dtype=np.int8)

    # Set starting trimbit values
    if type(start_bit) is int:
        # Apply start bit to all chips
        print('Applying supplied starting bit to all chips.')
        start_val = start_bit
        start_bit = {}
        for index in range(M_NUM_CHIPS):
            start_bit[index] = start_val

    elif type(start_bit) is dict and not start_bit:
        # Default option - start at 0 except for chip 2
        print('Including all default starting bit values.')
        start_bit = {}
        for index in range(M_NUM_CHIPS):
            start_bit[index] = 0
        start_bit[2] = 4

    else:
        print('Using all supplied starting bit values, and zero for the rest.')
        user_starts = start_bit.copy()
        start_bit = {}
        for index in range(M_NUM_CHIPS):
            start_bit[index] = 0

        # Replace defualts with user-supplied values
        for bit in user_starts.keys():
            start_bit[bit] = user_starts[bit]

    # Troubleshooting - delete when confirmed to work
    print('Start bits: ' + str(start_bit))

    # Default included_pixels, but skip bad pixels and chip boundaries only
    included_pixels = cal.get_included_pixels(key=key)

    for x_index in range(M_SIZE_X):
        for y_index in range(M_SIZE_Y):
            if not included_pixels[x_index, y_index]:
                # Automatically fail the fit if the data point is to be excluded
                trimfails[x_index, y_index] = 1
            else:
                # Pixel is to be included, set up the fit
                chip_num = pix.get_chip_coords(x_index, y_index)[0]
                trimstarts[x_index, y_index] = start_bit[chip_num]
                trims = trimbit[trimstarts[x_index, y_index]:]
                data = calibration_data[x_index, y_index, trimstarts[x_index, y_index]:]

                try:
                    """
                    Try the fit for a first time. This time we will be conservative about throwing
                    out "bad" pixels by using a relatively low chi-square limit. This is to help
                    find the pixels which suffer from anomalous noise at low trimbit values. The
                    default value of 6.0 was chosen after looking at the results of a preliminary fit.
                    """
                    fit_param, fit_cov = fit_pixel(trims, data, chi_limit=chi_limit)
                    trimfits[x_index, y_index, :] = fit_param
                    trimcov[x_index, y_index, :, :] = fit_cov

                except Exception:
                    if trimstarts[x_index, y_index] < max_start_bit:
                        # If possible try again, but with a restricted startbit
                        trimstarts[x_index, y_index] = max_start_bit
                        trims = trimbit[trimstarts[x_index, y_index]:]
                        data = calibration_data[x_index, y_index, trimstarts[x_index, y_index]:]

                        try:
                            # This time the chi-square limit is the default value, so data is only thrown out
                            # for really bad fits.
                            fit_param, fit_cov = fit_pixel(trims, data)
                            trimfits[x_index, y_index, :] = fit_param
                            trimcov[x_index, y_index, :, :] = fit_cov
                        except:
                            # Failed again - mark the fit as bad
                            print('Trimfit failed for pixel (' + str(x_index) + ', ' + str(y_index) + ')')
                            trimfails[x_index, y_index] = 1
                    else:
                        # If the start bit can't be restricted, try just raising the chi-square limit
                        try:
                            fit_param, fit_cov = fit_pixel(trims, data)
                            trimfits[x_index, y_index, :] = fit_param
                            trimcov[x_index, y_index, :, :] = fit_cov
                        except:
                            # Still does not work, mark the fit as bad
                            print('Trimfit failed for pixel (' + str(x_index) + ', ' + str(y_index) + ')')
                            trimfails[x_index, y_index] = 1

                except KeyboardInterrupt:
                    # Manually stop the whole program
                    print('Exiting calibration. Data has not been saved.')
                    raise SystemExit(0)

            # Counter to keep up with fitting progress
            if not quiet:
                count = (x_index)*(M_SIZE_Y) + y_index
                if count%1000 == 0:
                    print('{:4.0f}% done'.format(float(count)/(M_SIZE_X*M_SIZE_Y)*100.))

    print('Done.')
    return trimfits, trimcov, trimfails, trimstarts

def fit_pixel(trimbit, calibration_data, method='scipy', chi_limit=500.0,
              num_der=True, double=False):
    """
    Description
    ===============
        For a single pixel, fit the s-curve.
    Parameters:
    ===============
        - trimbit = (array) Trimbit setting for each measurement in calibration_data.
        - calibration_data = (array) Trimscan data (photon counts) for a single pixel.
        - method = (string) Identifier string for the fitting method to use. See below
            for options.
        - chi_limit = (float) Throws an error if the reduced chi-square for the fit is
            above this value.
        - num_der = (bool) Use numerical derivatives to seed the initial values for the
            nonlinear fitting routine. Users should generally use this.
        double = (bool) Fit a "double S-curve" to the data instead of a single S-curve.
            Useful for large energy-range calibrations. Not fully enabled at this time.
    Returns:
    ===============
        - popt = (array) The optimized parameters for the fit.
        - pcov = (array) The estimated covariance matrix for the fit.
    """
    # Guess the inflection point.
    if num_der:
        delta_x = 1
        data_slope = np.gradient(calibration_data, delta_x)
        index = np.argmin(data_slope)
    else:
        index = (np.abs(calibration_data - np.mean(calibration_data))).argmin()

    # Initial guess for the parameters
    p0 = [float(index), 1.0, calibration_data.max(), 0.0, calibration_data.min(), 0.0]

    if method == 'mpfit':
        """
        Use Novimir's mpfit module (originally ported from IDL) to perform the nonlinear fit. This
        library is fairly extensive and may require updating of deprecated features in order to
        run. Using the "scipy" method is recommended, but this is left for backwards compatibility.
        It also seems to fail with somewhat less frequency.
        """
        # I want to make sure that the background and charge shareing always have a negitive slope.
        parinfo = [{'fixed':0, 'limited':[0, 0], 'limits':[0., 0.]} for x in range(np.size(p0))]

        parinfo[3]['limited'] = [0, 1]
        parinfo[5]['limited'] = [0, 1]

        fkeys = {'x':trimbit, 'y':calibration_data}
        fit = mpfit.mpfit(s_curve_residual, p0, residual_keywords=fkeys, parinfo=parinfo, quiet=True)
        popt = fit.params
        pcov = fit.covar

    elif method == 'scipy':
        """
        This uses the scify.optimize.curve_fit function to perform the nonlinear trimbit S-curve
        fits. This method is recommended as it is based on a modern code and is expected to work
        "out of the box".
        """
        # Use bounds to enforce the slope parameters (A3 and A5) to be negative
        bounds = (np.array([-np.inf, -np.inf, -np.inf, -np.inf, -np.inf, -np.inf]),   # Lower bounds
                  np.array([np.inf, np.inf, np.inf, 0.0, np.inf, 0.0]))               # Upper bounds

        # Assume poisson statistics for uncertainty
        sigma = np.sqrt(calibration_data)

        # Use the scipy nonlinear optimization routine to perform the fits
        popt, pcov = curve_fit(s_curve_sp, trimbit, calibration_data, p0=p0, bounds=bounds,
                               sigma=sigma, absolute_sigma=True)

    # Raise an exception if the fit is low quality
    chi, dof = chi_sq(popt, trimbit, calibration_data)
    if (chi/dof) > chi_limit:
        raise RuntimeError('Large reduced chi-square value indicates a bad fit.')

    return popt, pcov

def s_curve(p, x=None):
    """
    Description
    ===============
        This defines the form of an s-curve which desribes the responce of the Pilatus detectors.
        This version accepts the parameters in array-form, as returned by scipy.optimize.curve_fit,
        and is therefore useful for examining the output of the fitting process.
    Parameters:
    ===============
        p = (float[6]) Array describing the s-curve parameters.
        x = (int) A trimbit value, 0-63.
    Returns:
    ===============
        (float) Detector responce at this trimbit setting with the given parameters.
    """
    return 0.5*(erf(-1*(x - p[0])/np.sqrt(2)/p[1]) + 1.)*(p[2] + p[3]*(x - p[0])) + \
           (p[4] + p[5]*(x - p[0]))

def s_curve_sp(x, A0, A1, A2, A3, A4, A5):
    """
    Description
    ===============
        This defines the form of an s-curve which desribes the responce of the Pilatus detectors.
        This version accepts th parameters individually, as is required to input into
        scipy.optimize.curve_fit. For examining output, see the above version of this function.
    Parameters:
    ===============
        p = (float[6]) Array describing the s-curve parameters.
        x = (int) A trimbit value, 0-63.
    Returns:
    ===============
        (float) Detector responce at this trimbit setting with the given parameters.
    """
    return 0.5*(erf(-1*(x - A0)/np.sqrt(2)/A1) + 1.)*(A2 + A3*(x - A0)) + \
           (A4 + A5*(x - A0))

def s_curve_residual(p, fjac=None, x=None, y=None, double=False):
    """
    Description
    ===============
    Define the residual function for to use when fitting the s-curve for the trimbits.
    Here we assume that the detected counts are Poisson distributed.

    This function is used with the mpfit routine, and should therefore be considered deprecated.
    See the mpfit documentation for a description of its argumanets. This is also the reason for
    the seemingly-unused arguments.
    """
    if double:
        fit = double_s_curve(p, x)
    else:
        fit = s_curve(p, x)

    return {'residual':np.sqrt((fit - y)**2)/np.sqrt(y+1e-9)}

def double_s_curve(p, x=None):
    """
    Description
    ===============
        This defines the form of an s-curve which desribes the responce of the Pilatus detectors.
        This version accepts the parameters in array-form, as returned by scipy.optimize.curve_fit,
        and is therefore useful for examining the output of the fitting process. This version of the
        S-curve is useful when the trimbit scan includes two emission lines, generally over a large
        energy range.
    Parameters:
    ===============
        - p = (float[9]) Array describing the s-curve parameters.
        - x = (int) A trimbit value, 0-63.
    Returns:
    ===============
        (float) Detector responce at this trimbit setting with the given parameters.
    """
    return 0.5*(erf(-1*(x - p[6])/np.sqrt(2)/p[8])+1.)*p[7] + \
           0.5*(erf(-1*(x - p[0])/np.sqrt(2)/p[1])+1.)*(p[2] + p[3]*(x - p[0])) + \
           (p[4] + p[5]*(x - p[0]))

def chi_sq_map(calibrations, quiet=False):
    """
    Description
    ===============
        Computes the chi-squared statistic of the S-curve fit for the every pixel on the detector.
        The result is returned as a "map" of the statistic across the detector.
    Parameters:
    ===============
        - calibration = (list of dictionaries) A chi-squared map will be computed for each element
            contained in the list. This is the set of dictionaries returned by the
            calibration.calibrate_detector function.
        - quiet = (bool) Suppress the output of the progress counter. Useful for integration in
            interactive environments, i.e. Jupyter notebooks.
    Returns:
    ===============
        - stat_maps = (dictionary) A dictionary containing the pixel maps of 'chi-squared',
            'reduced chi-squared', and 'dof'. These are pixel maps indexed by [element][x, y]
            in the detector frame.
    """
    stat_maps = {}

    for calib in calibrations:
        print('Calculating trimbit chi-squared map for '+calib['element'])
        chi_map = np.zeros([M_SIZE_X, M_SIZE_Y])
        dof_map = np.zeros([M_SIZE_X, M_SIZE_Y])
        red_map = np.zeros([M_SIZE_X, M_SIZE_Y])

        for x_index in range(M_SIZE_X):
            for y_index in range(M_SIZE_Y):
                # Mark failed fits for easy identification
                if not calib['trimfails'][x_index, y_index]:
                    params = calib['trimfits'][x_index, y_index]
                    trimbits = np.arange(calib['start bits'][x_index, y_index], 64)
                    data = calib['trimscan'][x_index, y_index, trimbits[0]:]

                    chi, dof = chi_sq(params, trimbits, data)
                    chi_map[x_index, y_index] = chi
                    dof_map[x_index, y_index] = dof
                    red_map[x_index, y_index] = chi / dof
                else:
                    chi_map[x_index, y_index] = -1
                    dof_map[x_index, y_index] = -1
                    red_map[x_index, y_index] = -1

                # Counter to keep up with fitting progress
                if not quiet:
                    count = (x_index)*(M_SIZE_Y) + y_index
                    if count%1000 == 0:
                        print('{:4.0f}% done'.format(float(count)/(M_SIZE_X*M_SIZE_Y)*100.))

        # Make a flattened array for easy histogramming, with bad fits removed
        flat_chi = chi_map.flatten()
        flat_chi = np.delete(flat_chi, np.where(flat_chi == -1))
        flat_red = red_map.flatten()
        flat_red = np.delete(flat_red, np.where(flat_red == -1))

        # Check for duplicate elements - no more than 2 of a given element string
        if not calib['element'] in stat_maps.keys():
            elem_key = calib['element']
        else:
            elem_key = calib['element'] + '_2'
            print('Element key alread exists. Storing as ' + elem_key)

        stat_maps[elem_key] = {'chi-squared': chi_map, 'reduced chi-squared': red_map,
                                       'dof': dof_map, 'chi-squared list': flat_chi,
                                       'reduced chi-squared list': flat_red}

    return stat_maps

def chi_sq(params, trimbits, data):
    """
    Description
    ===============
        Computes the chi-squared statistic for the S-curve fit for a single pixel. This
        assumes the data is drawn from a Poisson distribution, such that sigma = sqrt(data).
    Parameters:
    ===============
        - params = (array) The best-fit parameters for the given fit.
        - trimbits = (array) The trimbit values corresponding to the given data points.
            The independent variable (x) of the data set.
        - data = (array) Total photon counts for the pixel for each trimbit setting. The
            dependent variable (y) of the data set.
    Returns:
    ===============
        - chi = (float) The caluclated chi-squared statistic for the fit (not reduced).
        - dof = (int) The total degrees of freedom for the fit.
    """
    chi = np.sum((s_curve(params, trimbits) - data)**2 / data)
    dof = len(data) - len(params)

    return chi, dof
